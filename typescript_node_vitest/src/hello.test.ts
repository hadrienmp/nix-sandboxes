import { describe, expect, it } from "vitest"

const hello = (name: string = "world") => `Hello ${name}`

describe("hello", () => {
  it("world", () => {
    expect(hello()).toEqual("Hello world")
  })

  it("foo", () => {
    expect(hello("foo")).toEqual("Hello foo")
  })
})
