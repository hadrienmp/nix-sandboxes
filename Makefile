DIRENV_NIX_EXEC := direnv exec . nix develop --command
LANGUAGES := $(wildcard */.)


update_all: update_branch_create update_self $(LANGUAGES) update_branch_close


update_branch_create:
	git switch --create update

update_branch_close:
	git switch -
	git merge --no-edit update
	git branch --delete update


$(LANGUAGES):
	{ \
	LANGUAGE="$(shell basename $(shell realpath "$@"))" ;\
	MESSAGE="$$LANGUAGE: update dependencies" ;\
	if [ -f "$@/Makefile" ]; then \
		pushd "$@" ;\
		direnv allow ;\
		$(DIRENV_NIX_EXEC) \
			make update ;\
		$(DIRENV_NIX_EXEC) \
			git-gamble --pass --message "$$MESSAGE" || git restore . ;\
	fi \
	}


update_self:
	nix flake update
	$(DIRENV_NIX_EXEC) \
		git-gamble --pass --message 'base: update dependencies' -- true || true


warm_nix_cache:
	for LANGUAGE in . $(LANGUAGES); do \
		pushd "$$LANGUAGE" ;\
		direnv allow ;\
		$(DIRENV_NIX_EXEC) true ;\
		popd ;\
	done


update_and_warm: warm_nix_cache update_all warm_nix_cache


.PHONY: update_all $(LANGUAGES)
