:- begin_tests(hello).

:- use_module('hello').

test('hello world') :-
    assertion(hello("Hello world", unknown)).

test('hello foo') :-
    assertion(hello("Hello foo", "foo")).

:- end_tests(hello).
