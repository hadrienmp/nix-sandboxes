# Roc lang

[Roc](https://www.roc-lang.org/) is under construction, not even in alpha stage. It's an attempt to create a backend language inspired by the simplicity of Elm.

There is currently no editor, syntax highlighter or language server. The coffee script highlighter works a little.

You can checkout the [tutorial](https://www.roc-lang.org/tutorial)

## Use this template

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=roc/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#roc" ./your_new_project_directory
```

# How to

```bash
# Launch the tests
roc test

# Launch the tests in watch mode
make test-watch
```
