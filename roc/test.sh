#!/usr/bin/env sh

set -o xtrace
set -o errexit

# To prevent an infinite loop in watchmode
# roc format will touch the file and trigger a new loop
roc format --check *.roc || roc format *.roc
roc test
