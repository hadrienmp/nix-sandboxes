# TypeScript with formatting and test (Jest) on NodeJS

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=typescript_node_jest/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#typescript_node_jest" ./your_new_project_directory
```
