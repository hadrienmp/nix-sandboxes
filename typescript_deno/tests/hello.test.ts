import { describe, expect, it } from "./dev_deps.ts";

const hello = (name = "world"): string => `Hello ${name}`;

describe("hello", () => {
  it("world", () => {
    expect(hello()).toEqual("Hello world");
  });

  it("foo", () => {
    expect(hello("foo")).toEqual("Hello foo");
  });
});
