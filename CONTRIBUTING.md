# Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/nix-sandboxes/-/issues), [merge request](https://gitlab.com/pinage404/nix-sandboxes/-/merge_requests) ...) are welcome

## Adding a language

* copy an existing folder
* name the folder with the language
* adapt itt
* commit message should be `<language>: <a descriptive message>`
  * prefix your commit with
    * `<language>:` the name of the folder
    * `all:` when (almost) every languages are touched
    * `meta:` when it is about the project itself
    * `base:` when it touch the root folder
* in `<language>/flake.nix`, in the `packages`, add every dependencies needed
* `make <target>` must return 0 code on success, non 0 on fail
* complete the `<language>/Makefile` with
  * `make test`
  * `make lint` if possible
  * `make format` if possible
  * `make update` if there is specific steps to update the language and dependencies
  * `.PHONY` with what is needed
* in `<language>/.envrc`, set `GAMBLE_TEST_COMMAND` with the what is possible
  1. `make format lint test`
  1. `make format test`
  1. `make lint test`
  1. `make test`
* in `flake.nix` in the `templates` section, add the new template
* put the same description in
  * `flake.nix`'s `description`
  * `<language>/flake.nix`'s `description`
  * `<language>/README.md` title
* in `<language>/README.md`, change `<language>`
  * in the URL
  * in the flake command
* add an usage example of the language
  * using TDD
    * or even better using TCRDD (e.g. using [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md))
  * `hello` without argument or an optionnal (none) or empty string (depending on the language possibility) should output `Hello world`
  * `hello` with `foo` argument as string should output `Hello foo`
  * if you are a beginner with this language you may need to look at [help with language](./README.md#help-with-languages)
* in `<language>/.vscode/extension.json`, add needed extensions
