{ nixt }:

let
  hello = { name ? "world" }: "Hello ${name}";
in

nixt.mkSuites {
  "Hello" = {
    "world" = hello { } == "Hello world";
    "foo" = hello { name = "foo"; } == "Hello foo";
  };
}
